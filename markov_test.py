"""This module tests the module markov.py"""

import unittest
from markov import *

class TestMarkovChain(unittest.TestCase):

    """Testing class MarkovChain."""

    def test_matrix_fields(self):
        """
        Testing that a created transition matrix is valid:
            -each matrix cell has a value between 0.0 and 1.0
            -each matrix row adds up to 1.0
        """
        seq = "fuu"
        chain = MarkovChain(seq)
        for i in xrange(len(chain.matrix)):
            total = 0
            for j in xrange(len(chain.matrix)):
                self.assertEqual((0.0 <= chain.matrix[i,j] <= 1.0), True)
                total += j
            self.assertEqual(total, 1.0)

    def test_create_index(self):
        """Testing that all units from sequence have been appended to index."""
        seq = "fuu"
        chain = MarkovChain(seq)
        index = chain.create_index(seq)
        for i in seq:
            self.assertEqual(index.has_key(i), True)

    def test_normalize(self):
        """Testing that a matrix is normalized properly."""
        seq = "fuu"
        chain = MarkovChain(seq)
        chain.matrix[0, 0] = 5.0
        chain.matrix = chain.normalize_matrix(chain.matrix)
        for i in xrange(len(chain.matrix)):
            total = 0
            for j in xrange(len(chain.matrix)):
                total += chain.matrix[i,j];
            self.assertEqual(total, 1.0)

    def test_create_seq_end(self):
        """Testing that created sequence ends in correct unit."""
        seq = "fuu"
        chain = MarkovChain(seq)
        new_seq = chain.create_seq_end("f", "f")
        list = chain.to_list(new_seq)
        self.assertEqual(list[len(list)-1], "f")
    
    def test_wrapping_int(self):
        """Testing wrapping an int."""
        chain = MarkovChain("fuu")
        wrap = chain.to_list(123)
        self.assertEqual(list_int(wrap), 123)

    def test_wrapping_string(self):
        """Testing wrapping a string."""
        chain = MarkovChain("fuu")
        wrap = chain.to_list("fuu")
        self.assertEqual(list_str(wrap), "fuu")

if __name__ == '__main__':
    unittest.main()
