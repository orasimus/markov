"""Markov chain framework."""

from random import random
import numpy

class MarkovChain(object):

    """A Markov chain."""

    def __init__(self, source):
        """Creates a transition matrix & index dictionary from source."""
        self.index = {}
        self.matrix = self.create_transition_matrix(self.to_list(source))

    def create_transition_matrix(self, seq):
        """Creates a transition matrix."""
        self.index = self.create_index(seq)
        matrix = numpy.matrix(numpy.zeros(shape = (len(self.index), len(self.index))))
        for i in xrange(1, len(seq)):
            matrix[self.index[seq[i-1]], self.index[seq[i]]] += 1.0
        #If the last unit's row is 0.0, then add 1.0 chance to go to itself
        for i in xrange(len(matrix)):
            total = 0.0
            total += matrix[self.index[seq[len(seq)-1]], self.index[seq[i]]]
        if total == 0.0:
            matrix[self.index[seq[len(seq)-1]], self.index[seq[len(seq)-1]]] += 1.0
            
        return self.normalize_matrix(matrix)

    def create_index(self, seq):
        """Creates an index dictionary to use with the transition matrix."""
        index = {}
        index_no = 0
        for i in seq:
            if not(index.has_key(i)):
                index[i] = index_no
                index_no += 1
        return index

    def normalize_matrix(self, matrix):
        """Normalizes matrix rows."""
        for i in xrange(len(matrix)):
            total = 0
            for j in xrange(len(matrix)):
                total += matrix[i, j]
            if total != 0:
                for j in xrange(len(matrix)):
                    matrix[i, j] /= total
        return matrix

    def create_seq_len(self, init_s, length):
        """Create a sequence with given length."""
        seq = self.to_list(init_s)
        state = init_s
        for i in xrange(length-1):
            count = 0
            x = random()
            for j in self.index:
                count += self.matrix[self.index[state], self.index[j]]
                if x <= count:
                    seq.append(j)
                    state = j
                    break
        return seq

    def create_seq_end(self, init_s, end):
        """Create a sequence ending in given unit."""
        seq = self.to_list(init_s)
        state = init_s
        while True:
            if state == end:
                break
            count = 0
            rand = random()
            for i in self.index:
                count += self.matrix[self.index[state], self.index[i]]
                if rand <= count:
                    seq.append(i)
                    state = i
                    break
        return seq

    def stationary_distribution(self):
        """
        Gives a possible stationary distribution of the transition matrix.
        If any of the cells returned is zero, there is no stationary distribution.
        """
        transpose = numpy.matrix(self.matrix).T
        for i in xrange(len(transpose)):
            for j in xrange(len(transpose)):
                if i == j:
                    transpose[i, j] -= 1.0
        for j in xrange(len(transpose)):
            i = len(transpose)-1
            transpose[i, j] = 1
        array = numpy.array([0 for i in xrange(len(transpose))])
        array[len(array)-1] = 1
        return numpy.linalg.solve(transpose, array)
        

    def marginal_distribution(self, time):
        """Returns marginal distribution at a certain time."""
        return self.matrix**time

    def to_list(self, source):
        """Wraps a string, int or long into a list."""
        self.cn = source.__class__.__name__
        if self.cn == "int" or self.cn == "long":
            return map(int, list(str(source)))
        elif self.cn == "str":
            return list(source)
        elif self.cn == "list":
            return source

def list_int(list):
    """Transforms a list into an integer/long."""
    return int(''.join(map(str, list)))

def list_str(list):
    """Transforms a list into a string."""
    return ''.join(map(str, list))
